#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 *  AUTHOR : Simon Murier
 *
 *  NAME : checkResultRoulette
 *
 *  DESCRIPTION : Check if the player won his or not in the Roulette game
 *
 *  PARAMETERS : int resultRoulette     The result of the roulette Game from 0 to 36
 *               int bet                The number of tokens bet by the player
 *               int color              The color choosen by the player (1 for Red - 2 for Black)
 *
 *  RETURN :     Type : int
 *               Value : bet
 */
int checkResultRoulette(int resultRoulette, int bet, int color)
{
    int token;

    //Case where the result of the Roulette is 0 --> Lose
    if(resultRoulette == 0)
    {
        printf("\nDealer says - Result : %d, you lose your bet\n\n", resultRoulette);
        bet = 0;
    }

    //Case where the result of the Roulette is Pair/Red
    else if(resultRoulette%2 == 0)
    {
        //Case where the player also choose Red --> Win
        if(color == 1)
        {
            printf("\nDealer says - Result : %d, Red, you double your bet\n\n", resultRoulette);
            bet = bet*2;
        }

        //Case where the player choose Black --> Lose
        else
        {
            printf("\nDealer says - Result : %d, Red, you lose your bet\n\n", resultRoulette);
            bet = 0;
        }
    }

    //Case where the result of the Roulette is Odd/Black
    else
    {
        //Case where the player also choose Black --> Win
        if(color == 2)
        {
            printf("\nDealer says - Result : %d, Black, you double your bet\n\n", resultRoulette);
            bet = bet*2;
        }

        //Case where the player choose Red --> Lose
        else
        {
            printf("\nDealer says - Result : %d, Black, you lose your bet\n\n", resultRoulette);
            bet = 0;
        }
    }
    return bet;
}



/*
 *  AUTHOR : Simon Murier
 *
 *  NAME : roulette
 *
 *  DESCRIPTION : Allow the player to bet a specific amount of tokens on a color
 *
 *  PARAMETERS : int token              The number of tokens owned by the player
 *
 *  RETURN :     Type : int
 *               Value : token
 */
int roulette(int token)
{
    int color;
    int bet;
    int resultRoulette;
    srand(time(NULL));

    printf("Bet on ?\n Red[1]       Black[2]\n");
    scanf("%d", &color);

    //The player choose Red
    if(color == 1)
    {
        printf("How many token will you bet ?\n");
        scanf("%d", &bet);

        //Assert while player try to bet a wrong amount of tokens (too less, too much or more than owned)
        if(bet < 1)
        {
            printf("You can't bet less than 1 token.\n");
            system("pause");
            system("cls");
            return token;
        }
        else if(bet > 25)
        {
            printf("You can't bet more than 25 token.\n");
            system("pause");
            system("cls");
            return token;
        }
        else if(bet > token)
        {
            printf("You can't bet more token than you actually own.\n");
            system("pause");
            system("cls");
            return token;
        }

        //Reduce amount of token owned by the bet
        token = token - bet;

        //Randomly choose the result of the Roulette Game between 0 and 37
        printf("Dealer says - ""Bets are made"" !\n\n");
        resultRoulette = rand()%37;

        //Calling the function to check the result of the Roulette game and increment the amount of token won (or not)
        bet = checkResultRoulette(resultRoulette, bet, color);
        token = token + bet;
    }

    //The player choose Black
    else if(color == 2)
    {
        printf("How many token will you bet ?\n");
        scanf("%d", &bet);

        //Assert while player try to bet a wrong amount of tokens (too less, too much or more than owned)
        if(bet < 1)
        {
            printf("You can't bet less than 1 token.\n");
            system("pause");
            system("cls");
            return token;
        }
        else if(bet > 25)
        {
            printf("You can't bet more than 25 token.\n");
            system("pause");
            system("cls");
            return token;
        }
        else if(bet > token)
        {
            printf("You can't bet more token than you actually own.\n");
            system("pause");
            system("cls");
            return token;
        }

        //Reduce amount of token owned by the bet
        token = token - bet;

        //Randomly choose the result of the Roulette Game between 0 and 37
        printf("Dealer says - ""Bets are made"" !\n\n");
        resultRoulette = rand()%37;

        //Calling the function to check the result of the Roulette game and increment the amount of token won (or not)
        bet = checkResultRoulette(resultRoulette, bet, color);
        token = token + bet;
    }

    //The player choose an invalid color
    else
    {
        printf("Invalid color choice\n\n");
        system("pause");
        system("cls");
        return token;
    }

    return token;
}



/*
 *  AUTHOR : Simon Murier
 *
 *  NAME : russianRoulette
 *
 *  DESCRIPTION : Perform the Russian Roulette Game (only one turn)
 *
 *  PARAMETERS : int alive              The "actual state" of the player (0 = dead / 1 = Alive)
 *               int barrel[]            The barrel of the gun (an array of 6 integer)
 *
 *  RETURN :     Type : int
 *               Value : token
 */
int russianRoulette(int alive, int barrel[])
{
    printf("Pressing the trigger...\n\n");
    system("pause");
    system("cls");

    //Case where the player die
    if(barrel[5] == 1)
    {
        printf("Unlucky, you died...\n");
        alive = 0;
    }
    return alive;
}




/*
 *  AUTHOR : Simon Murier
 *
 *  NAME : playAgain
 *
 *  DESCRIPTION : Ask the player for another russian Roulette game
 *
 *  PARAMETERS : int choice             The choice of the player for another game (1 for Yes, 2 for No)
 *
 *  RETURN :     Type : int
 *               Value : choice
 */
int playAgain(int choice)
{
    //Ask the player and get his answer into the variable
    printf("Wanna play russian Roulette again with the same barrel ? [1]Yes    [2]No\n");
    scanf("%d", &choice);

    //switch of the player's choice
    switch(choice)
    {
    //Case where the player want to play another time
    case 1:
        choice = 1;
        break;

    //Case where the player doesn't want to play another game
    case 2:
        choice = 0;
        break;

    //Case where the player's entry is invalid, punish him by making him play another game with the same barrel
    default:
        printf("\nInvalid entry... so you gonna play again ! Stop trying to mess up my program !\n\n");
        choice = 1;
        break;
    }
    return choice;
}


int main()
{
    srand(time(NULL));

    int token = 10;
    int alive = 1;
    int choice = 0;

    int bullet = rand()%6;
    int barrel[6];

    printf("You trade your Rolex given by your aunt for your birthday and get %d jetons.\n\n", token);

    //Game goes on while the player is in debt
    while(token < 100)
    {
        //Calling function to play Roulette
        token = roulette(token);
        system("pause");
        system("cls");
        printf("You own %d tokens\n\n", token);

        if(token <= 0)
        {
            printf("No more tokens available, you're now playing russian Roulette.\n\n");

            choice = 1;
            bullet = rand()%6;
            int i;

            //Initialize a new empty barrel
            for(i = 0; i < 6; i++)
            {
                barrel[i] = 0;
            }

            //Loading a bullet in the barrel
            barrel[bullet] = 1;

            while(choice == 1)
            {
                //Perform a Russian Roulette game
                alive = russianRoulette(alive, barrel);

                //Case where the player is still alive
                if(alive == 1)
                {
                    //Change the position of the bullet in the barrel
                    barrel[bullet] = 0;
                    bullet++;
                    barrel[bullet] = 1;

                    //Increment tokens owned by the player
                    token = token + 20;
                    printf("You earn 20 tokens !\n\n");
                    system("pause");
                    system("cls");

                    //Ask the player for another game of Russian Roulette
                    choice = playAgain(choice);
                }

                //Case where the player ain't alive anymore - EXIT THE PROGRAM
                else if(alive == 0)
                {
                    printf("you lost your Rolex... and your life actually\n");
                    printf("LOSE !\n\n");
                    return 0;
                }

                system("pause");
                system("cls");

                //Display the number of tokens owned by the player
                printf("You own %d tokens\n\n", token);
            }
        }
    }

    //The player paid back his debt - EXIT THE PROGRAM
    printf("Congratulations ! You exit the casino alive, decide to give up your Rolex and then afford a Flik Flak\n");
    printf("WIN !\n\n");
    return 0;
}
