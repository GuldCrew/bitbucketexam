#include <stdio.h>
#include <stdlib.h>

#define MAX 5
int main()
{
    int cptr = 1;

    //Boucle while comptant de 1 � 10
    printf("Boucle while comptant de 1 a 10\n");
    while(cptr <= 10)
    {
        printf("%d ", cptr);
        cptr++;
    }

    printf("\n\n");

    //Boucle for comptant de 1 � 10
    printf("Boucle for comptant de 1 a 10\n");
    for(cptr = 1; cptr <= 10; cptr++)
    {
        printf("%d ", cptr);
    }

    printf("\n\n\n");

    //--------------------------------------------------------------

    printf("Une constante reserve une place en memoire et ne peut pas etre modifie par la suite.\n\nUn #define va chercher dans le code source toute les references correspondantes et les remplace par la valeur definie dans le define. Cette operation s'effectue a la compilation du programme.\n\n");

    printf("%d \n", MAX);

    const CONST = 18;
    //Le code en commentaire ci-dessous renvoi une erreur "Assignement sur une variable en lecture seule"
    //CONST = 16;
    printf("%d \n", CONST);

    //--------------------------------------------------------------

    //Diff�rence tentre switch et if
    int vote;
    printf("\nVoter pour [1]Melenchon  [2]Macron  [3]A bas la democratie !!!  [4]La reponse d\n");
    scanf("%d", &vote);

    //switch
    switch(vote)
    {
    case 1:
        printf("\nJ'ai vote Melenchon !\n\n");
        break;

    case 2:
        printf("\nPARCE QUE C'EST NOTRE PROJET !!!\n\n");
        break;

    case 3:
        printf("\nPour une France a l'image du village des Schtroumpfs !\n\n");
        break;

    case 4:
        printf("\nStephanie de Monaco !\n\n");
        break;

    default:
        printf("\n42... Juste 42\n\n");
        break;
    }

    //If
    if(vote == 1)
    {
        printf("\nJ'ai vote Melenchon !\n\n");
    }
    else if(vote == 2)
    {
        printf("\nPARCE QUE C'EST NOTRE PROJET !!!\n\n");
    }
    else if(vote == 3)
    {
        printf("\nPour une France a l'image du village des Schtroumpfs !\n\n");
    }
    else if(vote == 4)
    {
        printf("\nStephanie de Monaco !\n\n");
    }
    else
    {
        printf("\n42... Juste 42\n\n");
    }

    return 0;
}
